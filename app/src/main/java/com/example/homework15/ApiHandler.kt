package com.example.homework15

import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*


object ApiHandler {

    private var retrofit: Retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BASE_URL)
        .build()

    private var service: NetworkService = retrofit.create(NetworkService::class.java)

    interface NetworkService {
        @GET
        fun getRequest(@Url path: String): Call<String>

        @FormUrlEncoded
        @POST("{path}")
        fun postRequest(
            @Path("path") path: String,
            @FieldMap params: Map<String, String>
        ): Call<String>

        @PUT
        @FormUrlEncoded
        fun putRequest(@Url path: String, @FieldMap params: Map<String, String>): Call<String>
    }

    fun postRequest(path: String, params: Map<String, String>, callback: ApiCallback) {
        val call = service.postRequest(path, params)
        call.enqueue(onCallback(callback))
    }

    fun getRequest(path: String, callback: ApiCallback) {
        val call = service.getRequest(path)
        call.enqueue(onCallback(callback))
    }

    fun putRequest(path: String, params: Map<String, String>, callback: ApiCallback) {
        val call = service.putRequest(path, params)
        call.enqueue(onCallback(callback))
    }

    private fun onCallback(callback: ApiCallback): Callback<String> =
        object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                val status = response.code()
                if (status == HTTP_200_OK || status == HTTP_201_CREATED) {
                    val jsonObject = JSONObject(response.body()!!)
                    if (jsonObject.has("token")) callback.onSuccess(
                        jsonObject["token"].toString(),
                        response.code()
                    )
                    else callback.onSuccess(response.body().toString(), status)
                } else if (status == HTTP_400_BAD_REQUEST) {
                    try {
                        val jsonObject = JSONObject(response.errorBody()!!.string())
                        if (jsonObject.has("error")) {
                            callback.onError(jsonObject.getString("error"), status)
                        } else callback.onError("Bad request", status)
                    } catch (e: JSONException) {
                        callback.onError("Error while fetching data", status)
                    }
                } else if (status == HTTP_204_NO_CONTENT) callback.onError("No content", status)
                else if (status == HTTP_401_UNAUTHORIZED) callback.onError(
                    "Unauthorized please log in again",
                    status
                )
                else if (status == HTTP_404_NOT_FOUND) callback.onError(
                    "Resource not found",
                    status
                )
                else if (status == HTTP_500_INTERNAL_SERVER_ERROR) callback.onError(
                    "Internal server error",
                    status
                )
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                callback.onFail(t.toString())
            }
        }
}