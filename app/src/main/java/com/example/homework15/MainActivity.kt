package com.example.homework15

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity(), UserAdapter.CustomCallback {
    private val usersList = arrayListOf<UserModel>()
    private val adapter = UserAdapter(usersList, this)
    private var isEditing = false
    private var mPosition = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {
        val name = nameEditText
        val job = jobEditText
        addUserButton.setOnClickListener {
            if (isEditing) {
                if (name.text.toString().isNotEmpty() && job.text.toString()
                        .isNotEmpty()
                ) editUserUI(mPosition)
                else Toast.makeText(this, "Please fill all the fields", Toast.LENGTH_SHORT).show()

            } else {
                val params = mapOf<String, String>(
                    "name" to name.text.toString(),
                    "job" to job.text.toString()
                )
                if (name.text.toString().isNotEmpty() && job.text.toString().isNotEmpty()) addUser(
                    params
                )
                else Toast.makeText(this, "Please fill all the fields", Toast.LENGTH_SHORT).show()
            }
        }

        cancelButton.setOnClickListener {
            clear()
        }

        usersRecyclerView.layoutManager = LinearLayoutManager(this)
        usersRecyclerView.adapter = adapter
    }

    private fun clear() {
        val name = nameEditText
        val job = jobEditText
        addUserButton.text = "Add"
        addUserButton.isClickable = true
        isEditing = false

        name.text.clear()
        job.text.clear()

    }

    private fun editUserUI(position: Int) {
        val name = nameEditText
        val job = jobEditText

        progressBar.visibility = View.VISIBLE
        addUserButton.isClickable = false
        val params = mapOf("name" to name.text.toString(), "job" to job.text.toString())
        if (name.text.toString().isNotEmpty() && job.text.toString().isNotEmpty()) {
            ApiHandler.putRequest("$USERS/${usersList[position].id}", params, object : ApiCallback {
                override fun onSuccess(response: String, code: Int) {
                    val userModel = UserModel()
                    val jsonObject = JSONObject(response)
                    userModel.id = usersList[position].id
                    if (jsonObject.has("name")) userModel.name = jsonObject.getString("name")
                    if (jsonObject.has("job")) userModel.job = jsonObject.getString("job")
                    if (jsonObject.has("updatedAt")) userModel.updatedAt =
                        jsonObject.getString("updatedAt")
                    usersList[position] = userModel
                    adapter.notifyItemChanged(position)
                    clear()
                    cancelButton.visibility = View.GONE
                    updateDate(position)
                    progressBar.visibility = View.GONE
                }

                override fun onError(response: String, code: Int) {
                    Toast.makeText(this@MainActivity, response, Toast.LENGTH_SHORT).show()
                }

                override fun onFail(response: String) {
                    Toast.makeText(this@MainActivity, response, Toast.LENGTH_SHORT).show()
                }
            })
        } else Toast.makeText(this, "Please fill all the fields", Toast.LENGTH_SHORT).show()
    }


    override fun editUser(position: Int) {
        isEditing = true
        addUserButton.text = "Edit"
        cancelButton.visibility = View.VISIBLE
        mPosition = position
        val name = nameEditText
        val job = jobEditText

        name.setText(usersList[position].name)
        job.setText(usersList[position].job)
    }

    override fun deleteUser(id: String) {
        for (idx in 0 until usersList.size) {
            if (usersList[idx].id == id) {
                Toast.makeText(this, "User ${usersList[idx].name} removed", Toast.LENGTH_LONG)
                    .show()
                usersList.removeAt(idx)
                adapter.notifyItemRemoved(idx)
                break
            }
        }
    }

    private fun addUser(params: Map<String, String>) {
        addUserButton.isClickable = false
        progressBar.visibility = View.VISIBLE
        ApiHandler.postRequest(USERS, params, object : ApiCallback {
            override fun onSuccess(response: String, code: Int) {
                val userModel = UserModel()
                val jsonObject = JSONObject(response)
                if (jsonObject.has("name")) userModel.name = jsonObject.getString("name")
                if (jsonObject.has("job")) userModel.job = jsonObject.getString("job")
                if (jsonObject.has("id")) userModel.id = jsonObject.getString("id")
                if (jsonObject.has("createdAt")) userModel.createdAt =
                    jsonObject.getString("createdAt")
                usersList.add(userModel)
                adapter.notifyItemInserted(usersList.size - 1)
                clear()
                updateDate(usersList.size - 1)
                progressBar.visibility = View.GONE
            }

            override fun onError(response: String, code: Int) {
                Toast.makeText(this@MainActivity, response, Toast.LENGTH_SHORT).show()
            }

            override fun onFail(response: String) {
                Toast.makeText(this@MainActivity, response, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun updateDate(position: Int) {
        if (usersList[position].createdAt.isNotEmpty()) {
            val date = usersList[position].createdAt
            val model = usersList[position]

            val mDate = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(
                date.removeRange(
                    date.lastIndex - 4,
                    date.length
                )
            )
            val spf = SimpleDateFormat("HH:mm dd-MM-yyyy")
            val nDate = Date(mDate.time)
            val formattedDate = spf.format(nDate)
            model.createdAt = "Created at: $formattedDate"
            usersList[position] = model
            adapter.notifyItemChanged(position)
        } else {
            val date = usersList[position].updatedAt
            val model = usersList[position]

            val mDate = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(
                date.removeRange(
                    date.lastIndex - 4,
                    date.length
                )
            )
            val spf = SimpleDateFormat("HH:mm dd-MM-yyyy")
            val nDate = Date(mDate.time)
            val formattedDate = spf.format(nDate)
            model.createdAt = "Updated at: $formattedDate"
            usersList[position] = model
            adapter.notifyItemChanged(position)
        }
    }
}